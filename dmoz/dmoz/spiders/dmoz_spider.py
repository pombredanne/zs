from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from dmoz.items import DmozItem

class DmozSpider(BaseSpider):
    name = "dmoz"
    allowed_domains = ["dmoz.org"]
    start_urls = [
        "http://www.dmoz.org/Computers/Programming/Languages/Python/Books/",
        "http://www.dmoz.org/Computers/Programming/Languages/Python/Resources/"
    ]

    def parse(self, response):
#       filename = response.url.split("/")[-2]
#       open(filename, 'wb').write(response.body)

        hxs_persons = HtmlXPathSelector(response)
        sites = hxs_persons.select('//ul/li')

        event_items = []
        for site in sites:
            judge_mark_item = DmozItem()
            judge_mark_item['title'] = site.select('a/text()').extract()
            judge_mark_item['link'] = site.select('a/@href').extract()
            judge_mark_item['desc'] = site.select('text()').extract()

            event_items.append(judge_mark_item)
        return event_items