# Scrapy settings for yandex project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'yandex'

SPIDER_MODULES = ['yandex.spiders']
NEWSPIDER_MODULE = 'yandex.spiders'
DEFAULT_ITEM_CLASS = 'yandex.items.YandexItem'
COOKIES_DEBUG = True
#DOWNLOADER_MIDDLEWARES = {
#    'scrapy.contrib.downloadermiddleware.redirect.RedirectMiddleware': None,
#}
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'yandex (+http://www.yourdomain.com)'

