from flask import Flask
app = Flask('test.hello')

@app.route("/")
def hello():
    return "<html><body>Hello World!</body></html>"

if __name__ == "__main__":
    app.run()