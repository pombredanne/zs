CFG_PATH = 'scrapy.cfg'
DEFAULT_CFG = """[scrapyd]
debug=True
max_proc_per_cpu = 20

[settings]
default = dance_event.crawler.settings

[deploy]
#url = http://localhost:6800/
project = crawler
"""

def main():
    from scrapy.cmdline import execute
    import sys
    import os
    old_ssm = os.environ.get('SCRAPY_SETTINGS_MODULE', None)
    try:
        os.environ['SCRAPY_SETTINGS_MODULE']  = 'dance_event.crawler.settings'
        
        if not os.path.exists(CFG_PATH):
            with open(CFG_PATH, 'w') as cfg:
                cfg.write(DEFAULT_CFG)
        execute([sys.argv[0],]+['getevents',]+sys.argv[1:])
    finally:
        if old_ssm:
            os.environ['SCRAPY_SETTINGS_MODULE'] = old_ssm
        else:
            del os.environ['SCRAPY_SETTINGS_MODULE']
