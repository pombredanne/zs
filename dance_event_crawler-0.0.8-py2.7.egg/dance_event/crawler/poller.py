import os

from scrapyd.poller import QueuePoller
from .spiderqueue import UnlockedSqliteSpiderQueue
from scrapyd.utils import get_project_list


def get_spider_queues(config):
    """Return a dict of Spider Quees keyed by project name"""
    dbsdir = config.get('dbs_dir', 'dbs')
    if not os.path.exists(dbsdir):
        os.makedirs(dbsdir)
    d = {}
    for project in get_project_list(config):
        dbpath = os.path.join(dbsdir, '%s.db' % project)
        d[project] = UnlockedSqliteSpiderQueue(dbpath)
    return d

class QueuePoller(QueuePoller):
    def update_projects(self):
        self.queues = get_spider_queues(self.config)
