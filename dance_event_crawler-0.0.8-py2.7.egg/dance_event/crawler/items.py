# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field
from .models import (Dancer as Dancer_model,
                    Judge as Judge_model, 
                    Dance as Dance_model,
                    Competition as Competition_model,
                    PersonResult as PersonResult_model,
                    Heat as Heat_model,
                    Level as Level_model,)
class Person(Item):
    # define the fields for your item here like:
    given_name = Field()
    family_name = Field()
    gender = Field()

    def _name(self):
        return u' '.join((self.get('given_name', ''),
                          self.get('family_name', '')))


    def __eq__(self, other):
        return isinstance(other, self.__class__) and other._name() == self._name()
    
    def __hash__(self):
        return hash((self.__class__, self._name()))

    def __repr__(self):
        return "<%s person '%s', '%s'>"%(self.__class__.__name__,
                                        self.get('family_name', None),
                                        self.get('given_name', None))
    def __unicode__(self):
        return self._name()


class Dancer(Person):
    _model = Dancer_model


class Judge(Person):
    _model = Judge_model

class NameEq(object):
    def __eq__(self, other):
        return isinstance(other, self.__class__) and other['name'] == self['name']
    
    def __hash__(self):
        return hash((self.__class__, self['name']))

class Dance(Item, NameEq):
    _model = Dance_model
    name = Field()
    def __repr__(self):
        return "<Dance '%s'>"%self['name']


class Competition(Item):
    _model = Competition_model
    name = Field()
    date = Field()
    def __eq__(self, other):
        return isinstance(other, self.__class__) \
            and (other['name'] == self['name'] and\
                 other['date'] == self['date'])
    
    def __hash__(self):
        return hash((self.__class__, self['name'], self['date']))


class Level(Item, NameEq):
    _model = Level_model
    name = Field()


class Heat(Item, NameEq):
    _model = Heat_model
    name = Field()

    def __eq__(self, other):
        return isinstance(other, self.__class__) and other['name'] == self['name']
    
    def __hash__(self):
        return hash((self.__class__, self['name']))    


class PersonResult(Item):
    _model = PersonResult_model
    _hashattrs = ('dancer',
                'dancer_num',
                'judge',
                'competition',
                'level',
                'heat',
                'recall',
                'placement',
                'dance')

    dancer = Field()
    dancer_num = Field()
    judge = Field()
    competition = Field()
    level = Field()
    heat = Field()
    recall = Field()
    placement = Field()
    dance = Field()
    source = Field()

    def __eq__(self, other):
        attrs_equal = (other.get(attrname)==self.get(attrname) \
                                        for attrname in self._hashattrs)
        return isinstance(other, self.__class__) and all(attrs_equal)
    
    def __hash__(self):
        return hash((self.__class__,) \
                    + tuple((self.get(attrname) \
                                        for attrname in self._hashattrs)))


class PersonSet(set):
    def named(self, field, name):
        for p in self:
            if unicode(p['family_name']) == unicode(name):
                    return p

    def family_named(self, name):
        return self.named('family_name', name)
    