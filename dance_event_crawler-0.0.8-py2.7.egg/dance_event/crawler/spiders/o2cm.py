from itertools import izip, takewhile, dropwhile, islice, cycle, chain
import os
import sys
from pprint import pprint
from urlparse import urlparse, parse_qs
from datetime import datetime
import re
import time
import string
from pickle import loads

#value="Back to scoresheet" onclick="ShowBlock('SCORESHEET_CODE_16777217'


from scrapy.spider import BaseSpider
from scrapy.http import Request, FormRequest
from scrapy.selector import HtmlXPathSelector as ScrapyHtmlXPathSelector
from scrapy.http import TextResponse
from scrapy.conf import settings

from subspider import SpiderSerializer as subspider, Subspider

from dance_event.crawler.items import (Dancer,
                           Dance,
                           Competition,
                           Judge,
                           PersonResult,
                           PersonSet,
                           Heat,
                           Level)


#April 21st, 2012
EVENT_DATE_FORMAT = r'%B %d %Y'


def show_in_browser(text):


    BROWSER = 'chromium-browser' if not 'BROWSER' in globals() else globals()['BROWSER']
    from tempfile import NamedTemporaryFile
    from subprocess import Popen
    import codecs
    temp = None
    with NamedTemporaryFile(delete=False) as t:
        t = codecs.getwriter('utf8')(t)
        t.write(text)
        temp = t
    
    b = Popen('%s '%BROWSER+'file://'+temp.name, shell=True)
    b.wait()


def rtb(response):
    show_in_browser(response.body)        


class HtmlXPathSelector(ScrapyHtmlXPathSelector):

    def text(self, selector):
        for item in self.select(selector):
            yield ''.join(item.extract()).strip()


class O2CMEventSpider(Subspider):
    name = 'o2cm-event-spider'
    start_urls = [
                  # 'http://www.o2cm.com/Results/event2.asp?event=nwr05',
                  'http://www.o2cm.com/Results/event3.asp?event=scr12'
                ]
    event_date = datetime(2012,1,1)
    event_name = 'asdasf'
    base_url = 'http://www.o2cm.com/Results/'

    def copy_meta(self, response, **extra):
        response.meta.update(extra)
        return response.meta

    heat_marker = 'h5b'
    def parse(self, response):
        html = HtmlXPathSelector(response)
        competition = Competition(name=self.event_name, 
                                  date=self.event_date)
        yield competition
        for href in html.select('//td[@class="%s"]/a'%self.heat_marker):
            link = ''.join(href.text('@href'))
            heat = ' '.join([w for w in ''.join(href.text('text()')).split('(')[0].split() if w])
            heat = Heat(name=heat)
            yield Request(self.base_url+link,
                      callback=self.parse_scoresheet,
                      meta={'heat':heat,
                            'competition':competition}
                      )
            

    def parse_couple(self, selected):
        gender = iter(('male', 'female'))
        dancers = []
        couple = [''.join(s).split(',') for s in selected.extract()]
        for c in couple:
            for name in c:
                name = name.split('-',1)[0].strip()
                try:
                    given_name, family_name = (n.strip() for n in name.split(' ', 1))
                    yield Dancer(given_name=given_name,
                                 family_name=family_name,
                                 gender=next(gender))
                except ValueError:
                    pass

    def parse_judge(self, selected):
        for text in selected.extract():
            try:
                given_name, family_name = (n.strip() for n in text.split(',', 1)[0].split(' ', 1))
                yield Judge(given_name=given_name, family_name=family_name)
            except ValueError:
                pass

    def parse_participants(self, html):
        judges = {}
        dancers = {}
        parsers = {
                    u'Couples': (self.parse_couple, dancers),
                    u'Judges': (self.parse_judge, judges)
                }
        participants, parser = (None, None)
        for row in html.select('descendant::tr'):
            marker = ''.join((row.text('descendant::td[position()=last()]/text()'))).strip()
            # print [marker in parsers, parsers.keys(), marker]
            if marker in parsers:
                parser, participants = parsers[marker]
                continue
            
            if not parser:
                continue
            num = ''.join(row.text('descendant::td[position()=1]/text()')).strip()
            if not num or not num.isdigit():
                continue
            selected = row.select('descendant::td/a/text()')
            if not len(list(selected)):
                selected = row.select('descendant::td[position()=last()]/text()')
            participants[num] = list(parser(selected))

        for num, judgelist in [(n, j) for n,j in judges.iteritems()]:
            judges[num] = judgelist.pop()

        return judges, dancers


    def parse_level(self, html):
        level = ''.join(html.select('descendant::option[@selected]/text()').extract())
        if not level:
            level = ''.join(html.select('text()').extract()).split()[-1]
        level_links = html.select('descendant::option[not(@selected)]/@value').extract()
        level = Level(name=level.lower().capitalize().replace(' ','-'))
        return level, level_links

    def parse_judge_markers(self, headers, judges):
        markers = {}
        for i, head in enumerate(headers,1):
            head = head.strip()
            if not head:
                continue
            if head in judges:
                markers[i] = judges[head]
        return markers

    def parse_scoresheet(self, response):
        html = HtmlXPathSelector(response)
        judges, dancers = self.parse_participants(html.select('//table[@class="t1n"][position()=last()]'))
        level, other_levels = self.parse_level(html.select('//td[@class="h4"]'))
        if 'omit_other_levels' not in response.meta:
            parsed = urlparse(response.request.url)
            data = parse_qs(parsed.query)
            url = '%s://%s%s'%(parsed.scheme, parsed.netloc, parsed.path)
            for level_id in other_levels:
                data['selCount'] = [level_id,]
                yield FormRequest(url,
                                  meta=self.copy_meta(response,
                                                     omit_other_levels='1'),
                                  callback=self.parse_scoresheet,
                                  formdata=data)

        items = list(chain(judges.itervalues(), chain(*dancers.itervalues())))+[level,response.meta['heat']]
        for scoresheet in html.select('//table[@class="t1n"][position()!=last()]'):
            dance = Dance(name=''.join(scoresheet.\
                                    select('descendant::tr[position()=1]/td/text()').extract()))
            judge_markers = self.parse_judge_markers(scoresheet.\
                                    select('descendant::tr[position()=2]/td/text()').extract(), judges)
            
            if not judge_markers:
                continue

            items += dance,

            for row in scoresheet.select('descendant::tr[position()>2]'):
                num = ''.join(row.select('descendant::td[position()=1]/text()').extract()).strip()
                results = ((judge, ''.join(row.select('descendant::td[position()=%s]/text()'%pos).extract()).strip()) for pos, judge in judge_markers.iteritems())
                for judge, result in results:
                    result = int(result) if result.isdigit() else bool(result)
                    for dancer in dancers[num]:
                        recall = result if isinstance(result, bool) else None
                        placement = result if not isinstance(result, bool) else None

                        items += PersonResult(dancer=dancer,
                                           dancer_num=int(num),
                                           judge=judge,
                                           competition=response.meta['competition'],
                                           heat=response.meta['heat'],
                                           level=level,
                                           recall=recall,
                                           placement=placement,
                                           dance=dance,
                                           source=response.request.url
                                           ),

        for item in items:
            yield item
        



class O2CMSpider(BaseSpider):
    name = 'o2cm'
    start_urls = ['http://www.o2cm.com/Results/']
    event_row_marker = u't1n'

    def parse_date(self, datestr, year):
        if isinstance(self.since, basestring):
            self.since = datetime.strptime(loads(self.since), '%Y-%m-%d')
        if isinstance(self.until, basestring):
            self.until = datetime.strptime(loads(self.until), '%Y-%m-%d')


        month, day = [d.strip() for d in datestr.split()]
        day = ''.join([c for c in day if c.isdigit()])
        return datetime.strptime(' '.join((month, day, year)), EVENT_DATE_FORMAT)

    def parse(self, response):
        # rtb(response)
        html = HtmlXPathSelector(response)
        year = None

        for row in html.select('//table[position()=1]/tr'):
            if self.event_row_marker in row.select('@class').extract():
                link = ''.join(row.text('td/a/@href'))
                start_urls = [response.request.url + link,]
                event_date = self.parse_date(''.join(row.text('td[position()=3]/text()')), year)
                event_name = ''.join(row.text('td/a/text()'))

                if event_date >= self.since and event_date <= self.until:
                    yield subspider(O2CMEventSpider.name,
                                    headers=response.request.headers,
                                    event_date=event_date,
                                    start_urls=start_urls,
                                    event_name=event_name,
                                    event_url=response.request.url,
                                    setting='FEED_URI=%s'%self.settings['FEED_URI'],)
            else:
                year = ''.join(row.select('td/text()').extract()).strip()