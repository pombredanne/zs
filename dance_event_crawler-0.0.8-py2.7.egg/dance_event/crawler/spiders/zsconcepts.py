from tempfile import NamedTemporaryFile
from itertools import izip, takewhile, dropwhile, islice, cycle
import os
import sys
from pprint import pprint
from urlparse import urlparse
from datetime import datetime
import re
import time
import string

#value="Back to scoresheet" onclick="ShowBlock('SCORESHEET_CODE_16777217'


from scrapy.spider import BaseSpider
from scrapy.http import Request, FormRequest
from scrapy.selector import HtmlXPathSelector as ScrapyHtmlXPathSelector
from scrapy.http import TextResponse
from scrapy.conf import settings


from subspider import SpiderSerializer as subspider, Subspider

from dance_event.crawler.items import (Dancer,
                           Dance,
                           Competition,
                           Judge,
                           PersonResult,
                           PersonSet,
                           Heat,
                           Level)


#April 21st, 2012
EVENT_DATE_FORMAT = r'%B %d %Y'


def show_in_browser(text):


    BROWSER = 'chromium-browser' if not 'BROWSER' in globals() else globals()['BROWSER']
    from tempfile import NamedTemporaryFile
    from subprocess import Popen
    import codecs
    temp = None
    with NamedTemporaryFile(delete=False) as t:
        t = codecs.getwriter('utf8')(t)
        t.write(text)
        temp = t
    
    b = Popen('%s '%BROWSER+'file://'+temp.name, shell=True)
    b.wait()


def rtb(response):
    show_in_browser(response.body)        


class HtmlXPathSelector(ScrapyHtmlXPathSelector):

    def text(self, selector):
        for item in self.select(selector):
            yield ''.join(item.extract()).strip()

class ZSConceptsEventListSpider(Subspider):
    name = 'zsconcepts'
    start_urls = ['http://www.dance.zsconcepts.com/results/',]
    omit_markers = ['Results in PDF format', 'These are all in PDF form']

    def parse(self, response):
        yield Request('http://www.dance.zsconcepts.com/results/',
                      callback=self.parse_event_list)

    def classify_event(self, html):
        event_name = list(html.text('//table//h1//text()'))
        if len(event_name):
            return event_name[0].split('-')[0].strip(), Spider0_0.name

        event_name = list(html.text('//body/h1/text()'))
        if len(event_name):
            if len(list(html.select('//body/ul'))):
                return event_name[0], Spider4.name
            else:
                return event_name[0].split('-')[0].strip(), Spider0_1.name

        event_name = list(html.text('//body/div[@id="block_menu"]/p/font/strong/text()'))
        if len(event_name):
            return event_name[0], Spider1.name
        

        event_name = list(html.text('//body/center/h1/text()'))
        if len(event_name):
            return event_name[0], Spider2.name

        event_name = list(html.text('//td[@class="main"]/span[@class="title"]/text()'))
        if len(event_name):
            return event_name[0], Spider3.name

        event_name = list(html.text('//table[@class="body"]//table[@class="body"]//td/strong/text()'))
        if len(event_name):
            return event_name[0], Spider5.name

        return None, None
    
    def parse_date(self, datestr):
        datelist=datestr.split()
        datelist[1] = ''.join((c for c in datelist[1] if c.isdigit()))
        return datetime.strptime(' '.join(datelist), EVENT_DATE_FORMAT)

    def parse_event_list(self, response):
        html = HtmlXPathSelector(response)
        url = urlparse(response.request.url)
        server = '://'.join((url.scheme, url.netloc))
        i = 0
        for td in html.select('//td'):
            a = td.select('a')

            link, event_name = (''.join(a.select('@href').extract()).strip(),
                                ''.join(a.select('text()').extract()).strip())
            if not all((link, event_name)) or '://' in link:
                continue
            
            request = Request(''.join((server, link, '/')),
            callback=self.parse_event)
            
            request.meta['date'] = self.parse_date(td.text('following-sibling::td/text()').next())
            yield request
    
    def parse_event(self, response):
        if isinstance(self.since, basestring):
            self.since = datetime.strptime(loads(self.since), '%Y-%m-%d')
        if isinstance(self.until, basestring):
            self.until = datetime.strptime(loads(self.until), '%Y-%m-%d')
        
        html = HtmlXPathSelector(response)
        if any(marker in response.body for marker in self.omit_markers):
            return
        name, spider = self.classify_event(html)
        self.log('date: "%s", event: "%s", spider:"%s", url: "%s"'%(response.meta['date'], name, spider, response.request.url))
        yield subspider(spider,
                        headers=response.request.headers,
                        event_date=response.meta['date'], 
                        start_urls=[response.request.url,],
                        event_name=name,
                        setting='FEED_URI=%s'%self.settings['FEED_URI'],)
        



class ZSConceptsEventSpiderBase(Subspider):
    headers = {'Accept-Language': ['en'], 'Accept-Encoding': ['x-gzip,gzip,deflate'], 'Accept': ['text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'], 'User-Agent': ['crawler/1.0'], 'Cookie': ['session_id=836113'], 'Referer': ['http://www.dance.zsconcepts.com/results/']}
    event_date = datetime(2012, 1, 1)
    event_name = 'asdas'

    heat = re.compile(r"""^\S+\s?[\w\d]+\s?(:|(?=A\w-))\s*
                    (?P<level>(\S+(\s?(\S))*?))
                    \s*
                    [\(]?
                        (?P<dance>
                            ([Ff]ormation\s+[Tt]eam){1}|
                            ([Cc]ha\s+[Cc]ha){1}|
                            ([Vv]iennese\s+[Ww]altz){1}|
                            ([Pp]aso\s+[Dd]oble){1}|
                            ([\w/]+))?
                    [\)]?
                    \s*
                    (?P<heat>(
                             (([Qq]uarter)|([Ss]emi))?[-\s]?[Ff]inal){1}|
                             (([Ff]irst|[Ss]econd|[Tt]hird|[Ff]ourth)?[-\s][Rr]ound){1}
                             )?$""", 
                    re.X | re.U)

    heat_tokens = ('dance', 'level', 'heat')

    def safe(self, text):
        return unicode(text, "utf-8", errors="replace")        

    def parse_heat_string(self, heat):
        matched = self.heat.match(heat)
        return matched and dict(zip(self.heat_tokens, 
                                (matched.group(token) \
                                    if not token == 'heat' \
                                    else matched.group(token).lower().replace(' ','-').capitalize()\
                                    for token in self.heat_tokens)))\
               or matched

    def copy_meta(self, response, **extra):
        response.meta.update(extra)
        return response.meta

    def parse_judges(self, html):
        judges = {}
        for judge_str in html:
            try:
                marker, given_name, family_name = judge_str.split()
                judges[marker.strip()] = Judge(given_name=given_name.strip(),
                                               family_name=family_name.strip())
            except ValueError:
                pass

        return judges
    
    partners_tokens = ('num', 'male', 'female')
    partners = re.compile(r'^(?P<num>\d*)\s*(?P<male>.+)/(?P<female>.+)', re.U)

    def parse_partners(self, partners, dancers):
        matched = re.match(self.partners, partners.strip())
        num = matched.group('num')
        num = int(num) if num and num.isdigit() else num
        genders = iter(self.gender_order)
        for gender in self.partners_tokens[1:]:
            family_name = matched.group(gender).strip()
            dancer = dancers.family_named(family_name)
            g = next(genders)

            if not dancer and family_name:
                dancer = dancers.add(Dancer(family_name=family_name))

            if dancer:
                dancer['gender'] = g
                yield num, dancer
    
    gender_order = ('male', 'female')
    omit_dances = [u'Rule 11', u'Final summary']

class Spider0_0(ZSConceptsEventSpiderBase):
    #main type
    #http://www.dance.zsconcepts.com/results/bamjam2012/

    start_urls = ['http://www.dance.zsconcepts.com/results/bamjam2012/', 
    # 'http://www.dance.zsconcepts.com/results/valpo2012/', 'http://www.dance.zsconcepts.com/results/harvard-invitational2012/', 'http://www.dance.zsconcepts.com/results/arnold2012/', 'http://www.dance.zsconcepts.com/results/columbia2011/', 'http://www.dance.zsconcepts.com/results/holycross2012/', 'http://www.dance.zsconcepts.com/results/brandeis2012/', 'http://www.dance.zsconcepts.com/results/berkeley-classic2012/', 'http://www.dance.zsconcepts.com/results/penn2012/', 'http://www.dance.zsconcepts.com/results/uofm2012/', 'http://www.dance.zsconcepts.com/results/bu2012/', 'http://www.dance.zsconcepts.com/results/bing2012/', 'http://www.dance.zsconcepts.com/results/cornell2011/', 'http://www.dance.zsconcepts.com/results/njdsc2011ff/', 'http://www.dance.zsconcepts.com/results/stonybrook2009/', 'http://www.dance.zsconcepts.com/results/harvard-beginners2009/', 'http://www.dance.zsconcepts.com/results/dcdi2009/', 'http://www.dance.zsconcepts.com/results/yale2009/', 'http://www.dance.zsconcepts.com/results/columbia2009/', 'http://www.dance.zsconcepts.com/results/valpo2010/', 'http://www.dance.zsconcepts.com/results/brandeis2010/', 'http://www.dance.zsconcepts.com/results/bu2010/', 'http://www.dance.zsconcepts.com/results/uofm2010/', 'http://www.dance.zsconcepts.com/results/berkeley-classic2010/', 'http://www.dance.zsconcepts.com/results/arnold2010/', 'http://www.dance.zsconcepts.com/results/nyu2010/', 'http://www.dance.zsconcepts.com/results/holycross2010/', 'http://www.dance.zsconcepts.com/results/penn2010/', 'http://www.dance.zsconcepts.com/results/keystone2010/', 'http://www.dance.zsconcepts.com/results/njdsc2010sf/', 'http://www.dance.zsconcepts.com/results/harvard-invitational2010/', 'http://www.dance.zsconcepts.com/results/bbj2010/', 'http://www.dance.zsconcepts.com/results/notredame2010/', 'http://www.dance.zsconcepts.com/results/osu2010/', 'http://www.dance.zsconcepts.com/results/tufts2010/', 'http://www.dance.zsconcepts.com/results/nwu2010/', 'http://www.dance.zsconcepts.com/results/njdsc2010ff/', 'http://www.dance.zsconcepts.com/results/princeton2010/', 'http://www.dance.zsconcepts.com/results/cornell2010/', 'http://www.dance.zsconcepts.com/results/uconn2010/', 'http://www.dance.zsconcepts.com/results/yale2010/', 'http://www.dance.zsconcepts.com/results/calpoly2011/', 'http://www.dance.zsconcepts.com/results/bu2011/', 'http://www.dance.zsconcepts.com/results/valpo2011/', 'http://www.dance.zsconcepts.com/results/dcdi2010/', 'http://www.dance.zsconcepts.com/results/nyu2011/', 'http://www.dance.zsconcepts.com/results/rutgers2010/', 'http://www.dance.zsconcepts.com/results/berkeley-classic2011/', 'http://www.dance.zsconcepts.com/results/brandeis2011/', 'http://www.dance.zsconcepts.com/results/notredame2011/', 'http://www.dance.zsconcepts.com/results/keystone2011/', 'http://www.dance.zsconcepts.com/results/bamjam2011/', 'http://www.dance.zsconcepts.com/results/arnold2011/', 'http://www.dance.zsconcepts.com/results/holycross2011/', 'http://www.dance.zsconcepts.com/results/njdsc2011sf/', 'http://www.dance.zsconcepts.com/results/uofm2011/', 'http://www.dance.zsconcepts.com/results/harvard-invitational2011/', 'http://www.dance.zsconcepts.com/results/rutgers2011/', 'http://www.dance.zsconcepts.com/results/harvard-beginners2011/', 'http://www.dance.zsconcepts.com/results/princeton2011/', 'http://www.dance.zsconcepts.com/results/uconn2011/', 'http://www.dance.zsconcepts.com/results/wisconsin2011/', 'http://www.dance.zsconcepts.com/results/dcdi2011/', 'http://www.dance.zsconcepts.com/results/yale2011/'
    ]



    name = 'zsconcepts-event-spider-0-0'

    def get_scoresheets_url(self, response):
        return response.meta['url']+'Scoresheets.htm'
    
    def get_placements_url(self, response):
        return response.request.url+'Placements.htm'

    def parse(self, response):
        for entity in self.parse_competition(response, Competition(name = self.event_name,
                                                         date=self.event_date)):
            yield entity

    def parse_competition(self, response, competition):
        if competition:
            yield competition

        yield Request(self.get_placements_url(response),
                      callback=self.parse_dancer_names,
                      meta = self.copy_meta(response,
                              url = response.request.url,   
                              competition =competition))


    def parse_dancer_names(self, response):
        bodies = response.body.split('<body>')
        dancers = PersonSet()
        for body in bodies:
            html = HtmlXPathSelector(text='<html><body>'+self.safe(body))
            namestrings = html.text('//div[@id="block_menu"]/table/tr/td[1]/text()')
            if not namestrings:
                continue

            for namestr in namestrings:
                
                family_name, given_name = (name.strip() for name in namestr.rsplit(',',1))
                dancers.add(Dancer(given_name=given_name, family_name=family_name))
            
            yield Request(self.get_scoresheets_url(response),
                          callback=self.parse_scoresheets,
                          meta=self.copy_meta(response,
                                              dancers=dancers))
    
    scoresheet_markers_xpath = 'descendant::tr[position()=1]/td/text()'
    scoresheet_results_xpath = 'descendant::tr[position()>1]'
    def parse_scoresheets(self, response):
        html = HtmlXPathSelector(response)

        if not 'dancers' in response.meta:
            response.meta['dancers'] = PersonSet()

        for heat_line in html.select('//div[@id="block_menu"]//table//tr'):
            heat_description = self.parse_heat_string(next(heat_line.text('td[position()=1]/text()')))
            heat_id = next(heat_line.text('td[position()=last()]/input/@onclick')).split("'")[-2]
            level = Level(name=heat_description['level'])
            heat = Heat(name=heat_description['heat'])
            yield level
            yield heat
            for scoresheet_container in html.select('//div[@id="%s"]'%heat_id):
                judges = self.parse_judges(scoresheet_container.\
                                        text('following-sibling::div/p[position()=2]/text()'))
                
                for judge in judges.values():
                    yield judge
                
                for scoresheet_num, scoresheet in enumerate(scoresheet_container.select('descendant::table')):

                    dance = list(scoresheet.text('preceding-sibling::p/text()'))
                    if scoresheet_num == 0 and len(dance) == 1:
                        dance = heat_description['dance']
                    else:
                        dance = dance[-1].split('/')[0]

                    if dance in self.omit_dances:
                        continue

                    dance = Dance(name=dance)
                    yield dance
                    markers = scoresheet.text(self.scoresheet_markers_xpath)
                    markers = [judges[m] for m in takewhile(lambda x:x in judges.keys(),
                                                            islice(markers, 1, None))]

                    for result in scoresheet.select(self.scoresheet_results_xpath):

                        results = list(result.text('descendant::td/text()'))
                        dancers = list(self.parse_partners(results[0], response.meta['dancers']))
                        for num, dancer in dancers:
                            yield dancer                            
                        
                        placements = zip(markers, [True if r.strip().upper() == 'R' \
                                                                  else int(r.strip()) \
                                                                    if r.strip().isdigit() \
                                                                    else False\
                                                            for r in results[1:]])

                        for judge, recall in placements:
                            actual_recall = recall if isinstance(recall, bool) else None
                            placement = recall if not isinstance(recall, bool) else None
                            for num, dancer in dancers:
                                yield PersonResult(dancer = dancer,
                                                   dancer_num = num,
                                                   judge = judge,
                                                   recall = actual_recall,
                                                   placement = placement,
                                                   competition = response.request.meta['competition'],
                                                   level = level,
                                                   heat = heat,
                                                   dance = dance,
                                                   source = response.request.url)
            

class Spider0_1(Spider0_0):
    start_urls = ['http://www.dance.zsconcepts.com/results/holycross2009/',]
    name = 'zsconcepts-event-spider-0-1'
    event_name = 'Holy Cross DanceSport 2009 - Results'

    def get_scoresheets_url(self, response):
        return response.meta['url']+'Scoresheets_%s.htm'%response.meta['event_subname']
    
    def get_placements_url(self, response):
        return response.request.url+'Placements_%s.htm'%response.meta['event_subname']

    def parse(self, response):
        html = HtmlXPathSelector(response)
        self.event_name = [e_n.strip() for e_n in self.event_name.split('-')][0]
        for event_subname in html.text('//body/h2/text()'):
            competition =  Competition(name = ' '.join((self.event_name, event_subname)),
                                       date=self.event_date)
            response.meta['event_subname'] = event_subname
            for entity in self.parse_competition(Competition(name = self.event_name,
                                                             date=self.event_date),
                                                 response):
                yield entity

class Spider1(Spider0_0):
    start_urls = ['http://www.dance.zsconcepts.com/results/bbj2006/',]
    name = 'zsconcepts-event-spider-1'
    scoresheet_markers_xpath = 'td/text()'
    scoresheet_results_xpath = 'descendant::tr'
    def parse_competition(self, response, competition):
        yield competition
        response.meta['competition'] = competition
        for entity in self.parse_scoresheets(response):
            yield entity

class Spider2(Spider0_0):
    #omit
    start_urls=['http://www.dance.zsconcepts.com/results/dcdi2011/',]
    name = 'zsconcepts-event-spider-2'

class Spider3(ZSConceptsEventSpiderBase):
    start_urls = [
                 #'http://www.dance.zsconcepts.com/results/cdc2007/',
                 'http://www.dance.zsconcepts.com/results/bbj2005/',
                 ]
    name = 'zsconcepts-event-spider-3'

    def parse(self, response):
        html = HtmlXPathSelector(response)
        competition = Competition(name = self.event_name,date=self.event_date)
        for link in html.text('//table//table//td[@class="center"]/a/@href'):
            yield Request(response.request.url+link,
                      callback=self.parse_event_summary,
                      meta = self.copy_meta(response,
                                            event_url=response.request.url,
                                            folder_name=link,
                                            competition=competition))
    
    def parse_event_summary(self, response):
        html = HtmlXPathSelector(response)
        judges = {}

        for judge_row in html.select('//tr/td[text()="Adjudicators"][@colspan="3"]')\
                        .select('ancestor::table[position()=1]//tr[position()!=1]'):
            j = judge_row.text('td/text()')
            try:
                mark, (given_name, family_name) = next(j), (v.strip() for v in next(j).split())

                judges[mark] = Judge(given_name=given_name,
                                  family_name=family_name)
                
                yield judges[mark]

            except StopIteration:
                pass


        url_parts = list(response.request.meta['folder_name'].rpartition('.'))
        url_parts[-2] = 'csum'
        yield Request(response.request.meta['event_url']+'.'.join(url_parts),
                      callback=self.parse_scoresheet,
                      meta=self.copy_meta(response,
                                          judges=judges))
    
    def parse_couple(self, couple):
        dancers = []
        gender = iter(('male', 'female'))
        for namestr in couple.split('and'):
            splitted = namestr.split()
            given_name, family_name = splitted[0], ' '.join(splitted[1:])
            parsed_gender = next(gender)
            if given_name or family_name:
                dancers.append(Dancer(given_name=given_name,
                                      family_name=family_name,
                                      gender=parsed_gender))
        return dancers
            
    headers_before_dances = (u'CouplesSchool/Club', u'#', u'Rnd')
    class_to_key = {u'coupleName':'couple', 
                    u'centerHeaderBig':'num',
                    u'centerSubheader':'heat'}

    level_name_map = {u'F':'Final',
                      u'1/2':'Semi-final',
                      u'1/4':'Quarter-final',
                      u'1/8':'Eighth-final'}

    header_to_colname_caster = {'num':lambda self, x: int(x) if x.isdigit() else None,
                                'heat': lambda self, x: self.level_name_map[x] if x in self.level_name_map else x,
                                'couple': parse_couple}

    header_to_colname_stopper = (u'Total',)

    def parse_dances(self, html):
        for i, item in enumerate(html.select('td')):
            header = ''.join((item.select('text()').extract())).strip()
            if header in self.header_to_colname_stopper:
                break
            if header and not header in self.headers_before_dances:
                yield Dance(name=header)

    
    def parse_judge_positions(self, html, judges):
        markers = enumerate((marker for marker in html.text('td/text()')), 1)
        while True:
            l = [(judges[m[1]], m[0]) for m in takewhile(lambda x:x[1] in judges, markers)]
            if not l:
                break
            yield l

    def parse_row(self, html, markers, data={}):
        for cls, key in self.class_to_key.items():
            value = ''.join(html.text('descendant::*[contains(@class, "%s")]/text()'%cls))
            if value:
                data[key] = self.header_to_colname_caster[key].\
                                __get__(self, self.__class__)(value)

        if not 'dances' in data:
            data['dances'] = {}
        
        for dance in markers:
            if not dance in data['dances']:
                data['dances'][dance] = {}

            for judge, pos in markers[dance]:
                value = ''.join(html.\
                                text('descendant::*[contains(@class, "centerRound")][position()=%s]/text()'%pos))
                value = int(value) if value.isdigit() else value.isalpha()
                data['dances'][dance][judge] = value

        return data
    
    colspan_offset = 4

    def parse_scoresheet(self, response):
        competition = response.request.meta['competition']
        yield competition
        html = HtmlXPathSelector(response)
        level = Level(name=''.join(html.text('//span[@class="competition"]/text()'))\
                                .split('.')[-1].rsplit(' ',1)[0].strip())
        yield level
        scoresheet = None
        for table_marker in html.select('//td[text()="Couples"]'):
            scoresheet = table_marker.select('ancestor::table[position()=1]')
            break
        header_markers = iter(scoresheet.\
                            select('descendant::tr[@class="header"][position()=1]')).next()
        judge_markers = iter(scoresheet.\
                    select('descendant::tr[@class="subheader"][position()=1]')).next()
        
        parsed_dances = list(self.parse_dances(header_markers))
        judges = response.request.meta['judges']
        markers = dict(zip(parsed_dances,
                           self.parse_judge_positions(judge_markers,
                                                    judges)))
        
        results = {}
        dancers = PersonSet()
        dances = set()
        for row in scoresheet.select('descendant::tr[starts-with(@class, "row")]'):
            results = self.parse_row(row,
                                     markers,
                                     data = {'couple':results.get('couple', None),
                                             'num':results.get('num', None)})

            if not 'heat' in results or not results['heat']:
                continue
                        
            results['heat'] = Heat(name=results['heat'])
            yield results['heat']
            
            for dancer in results['couple']:
                yield dancer
            
            for dance in results['dances']:
                if not dance in dances:
                    yield dance
                    dances.add(dance)
                for judge, result in results['dances'][dance].items():
                    for dancer in results['couple']:
                        recall = result if isinstance(result, bool) else None
                        placement = result if not isinstance(result, bool) else None
                        yield PersonResult(dancer=dancer,
                                           dancer_num=results['num'],
                                           judge=judge,
                                           competition=competition,
                                           heat=results['heat'],
                                           level=level,
                                           recall=recall,
                                           placement=placement,
                                           dance=dance,
                                           source=response.request.url
                                           )
            


class Spider4(Spider0_0):
    start_urls = ['http://www.dance.zsconcepts.com/results/bbj2007/',]
    name = 'zsconcepts-event-spider-4'
    def get_placements_url(self, response):
        return response.request.url+'Results.htm'

from threading import Lock
SPIDER_5_GLOBAL = {}
S5_LOCK = Lock()
class Spider5(ZSConceptsEventSpiderBase):
    start_urls = ['http://www.dance.zsconcepts.com/results/gamecock2008/',]
    name = 'zsconcepts-event-spider-5'
    event_date = datetime(2012, 1, 1)
    event_name = 'Gamecock Invitational 2008'

    def parse_dances(self, html):
        dances = {}
        for row in html.select('descendant::tr[position()!=1]'):
            mark, dance = row.text('descendant::td/text()')
            dances[mark] = Dance(name=dance)
        return dances

    def parse(self, response):
        html = HtmlXPathSelector(response)
        dances = self.parse_dances(html.select('//table[@bgcolor="white"]//td[@valign="top"]//table[@class="body"]//table[@class="body"]'))
        competition = Competition(name=self.event_name,
                                  date=self.event_date)
        yield competition
        for link in html.text('//table[@class="body"]//tr/td/a/@href'):
            yield Request(response.request.url+link,
                     meta=self.copy_meta(response,
                                        competition=competition,
                                        event_url=response.request.url,
                                        dance_markers=dances),
                     callback=self.parse_scoresheet)
            

    def parse_couple(self, couple):
        gender = iter(('male', 'female'))
        c = []
        for namestr in couple.split('&'):
            namelist = [n.strip() if n.strip() else '' for n in namestr.strip().split(' ', 1)]
            kwargs = dict(zip(('given_name', 'family_name'), namelist))
            kwargs['gender'] = next(gender)
            c.append(Dancer(**kwargs))
        return c

    def parse_scoresheet(self, response):
        global SPIDER_5_GLOBAL
        if not 'couples' in SPIDER_5_GLOBAL:
            with S5_LOCK:
                SPIDER_5_GLOBAL['couples'] = {}

        if not 'results' in SPIDER_5_GLOBAL:
            with S5_LOCK:
                SPIDER_5_GLOBAL['results'] = {}

        html = HtmlXPathSelector(response)
        dance_markers = response.request.meta['dance_markers']
        for dance in dance_markers.itervalues():
            yield dance

        heat = list(html.select('//table/strong/text()').extract())[-1]\
                    .split('.')[-1].rsplit(' ', 1)[0].strip()
        heat = Heat(name=heat)

        judges = {}
        for judge_row in html.select('//th[contains(text(),"Judges")]/ancestor::table[position()=1]/tr[position()!=1]'):
            mark, name = list(judge_row.text('descendant::td/text()'))
            given_name, family_name = name.split()
            judges[mark] = Judge(given_name=given_name, family_name=family_name)

        for scoresheet_head in html.select('//td[@bgcolor]'):
            head = [word.strip() for word \
                        in ''.join(scoresheet_head.select('descendant::text()').extract()).rsplit('-', 1)]
            if head[-1] in dance_markers:
                dance = dance_markers[head[-1]]
            else:
                dance = Dance(name=head[-1])
                yield dance

            level = Level(name=head[0].replace(' ','-').lower().capitalize() if len(head)>1 else u'Final')
            scoresheet = scoresheet_head.select('ancestor::table[position()=1]')
            judge_markers = {}

            for i, mark in enumerate(scoresheet.select('descendant::tr[position()=2]/td/text()'), 2):
                mark = mark.extract()
                if mark in judges:
                    judge_markers[i] = judges[mark]

            if not judge_markers:
                for i, mark in enumerate(scoresheet.select('descendant::tr[position()=1]/td[position()!=1]/strong/text()'), 2):
                    mark = mark.extract()
                    if mark in judges:
                        judge_markers[i] = judges[mark]
                rows = scoresheet.select('descendant::tr[position()!=1]')
            else:
                rows = scoresheet.select('descendant::tr[position()>2]')

            for row in rows:
                couple = ()
                couple_sel = row.select('descendant::td[position()=1]')
                couple_str = ''.join(couple_sel.select('text()').extract()).strip()
                num_str = ''.join(couple_sel.select('descendant::strong/text()').extract()).strip()

                if not num_str:
                    num_str = couple_str

                num = int(num_str.replace('.',''))
                if couple_str and not couple_str.isdigit():
                    couple = self.parse_couple(couple_str)
                    with S5_LOCK:
                        SPIDER_5_GLOBAL['couples'][num] = couple

                with S5_LOCK:
                    if not num in SPIDER_5_GLOBAL['results']:
                        SPIDER_5_GLOBAL['results'][num] = []
                
                    for i, judge in judge_markers.iteritems():
                        result = ''.join(row.select('descendant::td[position()=%s]/text()'%i).extract())
                        result = int(result) if result.isdigit() else bool(result)
                        SPIDER_5_GLOBAL['results'][num].append((response.meta['competition'],
                                                                level,
                                                                heat,
                                                                dance,
                                                                judge,
                                                                result))
        items = []
        S5_LOCK.acquire()
        try:
            for num, couple in SPIDER_5_GLOBAL['couples'].iteritems():
                if num in SPIDER_5_GLOBAL['results']:
                    items += couple
                    for results in SPIDER_5_GLOBAL['results'][num]:
                        for dancer in couple:
                            competition, level, heat, dance, judge, result = results
                            recall = result if isinstance(result, bool) else None
                            placement = result if not isinstance(result, bool) else None

                            items += PersonResult(dancer=dancer,
                                               dancer_num=num,
                                               judge=judge,
                                               competition=competition,
                                               heat=heat,
                                               level=level,
                                               recall=recall,
                                               placement=placement,
                                               dance=dance,
                                               source=response.request.url
                                               ),
                    del SPIDER_5_GLOBAL['results'][num]

        finally:
            S5_LOCK.release()

        for item in items:
            yield item