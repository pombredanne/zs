from sqlalchemy import (Column,
                        Integer,
                        String,
                        Date,
                        Boolean)
from sqlalchemy.ext.declarative import declarative_base, AbstractConcreteBase
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref

class Base(declarative_base()):
    __abstract__ = True
    _id = Column('id', Integer, primary_key=True)


    def __init__(self, **fields):
        for name, value in fields.iteritems():
            setattr(self, name, value)


class Person(Base):
    __abstract__ = True
    given_name = Column(String(256), nullable=True)
    family_name = Column(String(256), nullable=True)
    gender = Column(String(256))


class Judge(Person):
    __tablename__ = 'judges'


class Dancer(Person):
    __tablename__ = 'dancers'


class Competition(Base):
    __tablename__ = 'competitions'
    name = Column(String(256))
    date = Column(Date)


class Level(Base):
    __tablename__ = 'levels'
    name = Column(String(256))


class Heat(Base):
    __tablename__ = 'heats'
    name = Column(String(256))


class Dance(Base):
    __tablename__ = 'dances'
    name = Column(String(256))


class PersonResult(Base):
    __tablename__ = 'results'

    dancer_id = Column(Integer, ForeignKey('dancers.id'))
    dancer = relationship("Dancer", backref=backref('results', order_by=Base._id))

    judge_id = Column(Integer, ForeignKey('judges.id'))
    judge = relationship("Judge", backref=backref('results', order_by=Base._id))

    competition_id = Column(Integer, ForeignKey('competitions.id'))
    competition = relationship("Competition", backref=backref('results', order_by=Base._id))

    level_id = Column(Integer, ForeignKey('levels.id'))
    level = relationship("Level", backref=backref('results', order_by=Base._id))

    heat_id = Column(Integer, ForeignKey('heats.id'))
    heat = relationship("Heat", backref=backref('results', order_by=Base._id))

    dance_id = Column(Integer, ForeignKey('dances.id'))
    dance = relationship("Dance", backref=backref('results', order_by=Base._id))

    source = Column(String(256))

    dancer_num = Column(Integer, nullable=True)
    recall = Column(Boolean, nullable=True)
    placement = Column(Integer, nullable=True)